extends Node2D

var score = 0 setget set_score, get_score
var gameOver = false

func set_score(value):
	score = value
	get_node("Score").set_text("Score: " + str(score))
	
func get_score():
	return score	

func _ready():
	set_fixed_process(true)
	set_process_input(true)

func _fixed_process(delta):
	if(get_score() < 0):
		get_node("newGameLabel").set_text("Game over")
		gameOver = true
	
	if(get_node("Bricks").get_child_count() == 0):
		get_node("newGameLabel").set_text("You win! Your score is " + str(get_score()))						
														
func _input(event):
	if gameOver == true:
		if event.type == InputEvent.KEY:
			if event.scancode == KEY_Y:
				get_node("newGameLabel").set_text("")
				set_score(0)